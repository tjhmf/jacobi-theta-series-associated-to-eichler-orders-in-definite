def H(N1,N2, D):
    """
    Return the $(N_1,N_2)$-modified Hurwitz class number of $D$
    (see Yan Bin's thesis).
    """
    assert 0 != moebius(N1) and  0 != moebius(N2) and 1 == gcd(N1,N2),\
                'Error: (%d,%d) is not a pair of relatively prime squarfree integers'%(N1,N2)
    pf1 = prime_factors(N1)
    pf2 = prime_factors(N2)
    if 0 == D:
        return -(prod( (1-p) for p in pf1) * prod( (1+p) for p in pf2))/12
    else:
        is_disc = lambda d:(d%4==3 or d%4==0)
        Dp = D
        f = dict()
        for p in pf1 + pf2:
            f[p] = 1
            while (p^2).divides(Dp) and is_disc(Dp/p^2):
                     Dp = Dp/p^2
                     if p.divides(N2):
                                f[p] *= p
                     else:
                                f[p] = f[p]
        P1 = prod( (1-kronecker_symbol(-Dp,p)) for p in pf1)
        P2 = prod( (2*p*f[p]-p-1 - kronecker_symbol(-Dp,p)*(2*f[p]-p-1))/(p-1) for p in pf2)
        return QQ(gp.qfbhclassno(Dp) * P1 * P2)

def h_class_number(N,F):
    """
    Return the class number of left ideal belonging to an Eichler order of level $F$ in
    definite quaternion over $\Q$ which is ramified exactly at those finite primes which divide $N$.
    """
    primes_N = N.prime_divisors()
    primes_F = F.prime_divisors()
    e_N = len( primes_N)
    e_NF = e_N + len(primes_F)
    l = lambda p: kronecker_symbol(-4,p)
    h_NF = H(N,F,0)
    h_NF += prod( (1-l(p)) for p in primes_N)*prod( (1+l(p)) for p in primes_F)/4
    l = lambda p: kronecker_symbol(-3,p)
    h_NF += prod( (1-l(p)) for p in primes_N)*prod( (1+l(p)) for p in primes_F)/3
    
    return h_NF

def yb_type_number( N, F):
    """
    Return the type number of the Eichler orders of level $F$ in the definite quaternions algebra
    which ramifies exactly at the primes dividing $N$ using the formula from Yan Bin's thesis.
    """
    primes_N = N.prime_divisors()
    primes_F = F.prime_divisors()
    e_N = len( primes_N)
    e_NF = e_N + len(primes_F)
    l = lambda p: kronecker_symbol(-4,p)
    h_NF = H(N,F,0)
    h_NF += prod( (1-l(p)) for p in primes_N)*prod( (1+l(p)) for p in primes_F)/4
    l = lambda p: kronecker_symbol(-3,p)
    h_NF += prod( (1-l(p)) for p in primes_N)*prod( (1+l(p)) for p in primes_F)/3
    tn1 = h_NF/(2^e_NF)

    e = lambda m: sum( H(N,F,4*m-r^2) for r in range(-isqrt(4*m),1+isqrt(4*m)) if (m).divides(r))
    tn2 = sum( e(m) for m in divisors( N*F) if m != 1)/(2^(e_NF+1))

    return tn1 + tn2

def pizer_type_number( N, F):
    """
    Return the type number of the Eichler orders of level $F$ in the definite quaternions algebra
    which ramifies exactly at the primes dividing $N$ using the formula in~\cite[Thm. B]{Pizer}.
    """
    primes_N = N.prime_divisors()
    primes_F = F.prime_divisors()
    e_N = len(primes_N)
    e_NF = e_N + len(primes_F)
    l = lambda p: kronecker_symbol(-4,p)
    h_NF = H(N,F,0)
    h_NF += prod( (1-l(p)) for p in primes_N)*prod( (1+l(p)) for p in primes_F)/4
    l = lambda p: kronecker_symbol(-3,p)
    h_NF += prod( (1-l(p)) for p in primes_N)*prod( (1+l(p)) for p in primes_F)/3
    tn1 = h_NF/(2^e_NF)
    
    e1 = lambda m: len( m.prime_divisors())
    
    def sigma(m):
        if m%4 == 1:
           return 1
        elif m%4==2 or 3:
           return 0
    
    def disc(m):
        K = QuadraticField(-m, 'x')
        return K.disc()
    
    def classnumber(m):
        K = QuadraticField(-m, 'x')
        return K.class_number()
        
    PP1 = lambda m: prod((1-kronecker_symbol(disc(m),p)) for p in primes_N)
    PP2 = lambda m: prod((1+kronecker_symbol(disc(m),p)) for p in primes_F)
    PP_2 = lambda m: prod((1+kronecker_symbol(disc(m),p)) for p in primes_F if p != 2)
    
    def delta(m):
        if (m).divides(N*F) and (PP1(m)*PP2(m)) !=0:
           return 1
        else:
           return 0
    
    if N%2 == 0 and F%2 != 0 :
       t3 = lambda m: (delta(m)*classnumber(m))/(2^(e1(m)+ sigma(m)+ 1) )
       tn3 = sum( t3(m) for m in divisors( N*F) )

       return tn1 + tn3

    def tau(m):
        if m%4 == 1:
           return 1
        elif m%8 == 3:
           return -1
        elif m%8 == 7:
           return  0

    if N%2 != 0 and F%2 != 0 :
       t4 = lambda m: (delta(m)*classnumber(m) )/(2^(e1(m)+ tau(m)) )
       tn4 = delta(3)/2 + sum( t4(m) for m in divisors( N*F) if m>3)
       
       return tn1 + tn4

    def delta_(m):
        if (m).divides(N*F) and (PP1(m)*PP_2(m)) != 0:
           return 1
        else:
           return 0
            
    if N%2 != 0 and F%2 == 0 :
       t5 = lambda m: (delta(m)*classnumber(m) )/(2^(e1(m)+ sigma(m)+1))
       t51 = lambda m: (delta_(m)*classnumber(m))/(2^(e1(m)+1) )
       tn5 = delta_(3)/4 + sum( t5(m) for m in divisors( N*F)) +sum( t51(m) for m in divisors( N*F) if m%8==7) +sum( 3*t51(m) for m in divisors( N*F) if m%8==3 and m>3)
       
       return tn1 + tn5


L = [[N,F,h_class_number(N,F),yb_type_number( N, F),pizer_type_number( N, F), yb_type_number( N, F)-pizer_type_number( N, F)] for N in [1..210] for F in [1..210] if N*F<=210 and moebius(N)==-1 and moebius(F)!=0 and gcd(N,F)==1]

table(L,header_row=["$N$","$F$","$h$","$yb$", "$pizer$","$yb-pizer$"])

